import { render, screen } from "../test-utils";
import Index from "@/pages/index";

describe("Welcome Page", () => {
  it("should render the heading", () => {
    render(<Index />);

    const tagline = screen.getByText(
      /Play Traditional Game/i
    );
    expect(tagline).toBeInTheDocument();
  });
});