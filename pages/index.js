import Footer from '../components/footer';
import Header from '../components/header';
import LpGames from '../components/landingpage/lpgames';
import LpHeader from '../components/landingpage/lpheader';
import LpGameChoice from '../components/landingpage/lpgamechoice';
import LpRequirements from '../components/landingpage/lprequirements';
import LpTopScore from '../components/landingpage/lptopscore';
import LpSubscribe from '../components/landingpage/lpsubscribe';

const Index = () => {
  return (
    <>
      <Header />

      <main className="bg-black">
        <LpHeader />
        <LpGames />
        <LpGameChoice />
        <LpRequirements />
        <LpTopScore />
        <LpSubscribe />
      </main>

      <Footer />
    </>
  );
};
export default Index;
