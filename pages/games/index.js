import { Container, Row, Image,  } from "react-bootstrap";
import Footer from "../../components/footer";
import Header from "../../components/header";
import Link from "next/link";
import axios from "axios";

const Index = (props) => {
  const games = props.data.games;

  return (
    <div className="bg-black">
        <Header />
        <Container>
          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">Popular</h3>
            </div>
          </Row>

          <Row>
            {
              games.map(game => (
                game.play_count > 100 ? (
                  <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
                    <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                      <Image
                        src={
                          game.thumbnail_url != null ? game.thumbnail_url : ('https://via.placeholder.com/150')
                        }
                        className="card-img-top" style={{ borderRadius: '15px' }}
                        alt="thumbnail"
                      />
                      <div className="card-body">
                        <Link href={`/games/${game.slug}`}>
                          <a className="text-decoration-none">
                            <h5 className="card-title text-white">{game.name}</h5>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  ) : ('')
              ))
            }

          </Row>

          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">New</h3>
            </div>
          </Row>

          <Row>
            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

            <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
              <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: '15px' }}
                  alt="poster"
                />
                <div className="card-body">
                  <Link href={`/games/1`} >
                    <a className="text-decoration-none">
                      <h5 className="card-title text-white">Card title</h5>
                    </a>
                  </Link>
                </div>
              </div>
            </div>

          </Row>

          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">Sport</h3>
            </div>
          </Row>

          <Row>
          {
              games.map(game => (
                game.category === 'sport' ? (
                  <div className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2">
                    <div className="card border-0 bg-black" style={{ width: '18rem' }}>
                      <Image
                        src={
                          game.thumbnail_url != null ? game.thumbnail_url : ('https://via.placeholder.com/150')
                        }
                        className="card-img-top" style={{ borderRadius: '15px' }}
                        alt="poster"
                      />
                      <div className="card-body">
                        <Link href={`/games/${game.slug}`}>
                          <a className="text-decoration-none">
                            <h5 className="card-title text-white">{game.name}</h5>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  ) : ('')
              ))
            }

          </Row>

        </Container>
        <Footer />
  </div>
  )
}

export default Index;

export const getStaticProps = async () => {
  const res = await axios.get(`${process.env.NEXT_PUBLIC_API}/api/games`);

  return {
    props: { data: res.data }
  }
}