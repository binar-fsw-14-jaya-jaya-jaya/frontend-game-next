import Link  from 'next/link';
import { Container, Row, Image, Table  } from "react-bootstrap";
import Footer from "../../components/footer";
import Header from "../../components/header";
import { useRouter } from 'next/router';
import React from 'react';
import axios from 'axios';


const  Games = () => {
  const router = useRouter()
  const { slug } = router.query;
  const [game, setGame] = React.useState(null);

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API}/api/games/${slug}`)
      setGame(response.data.game);
    };

    if (slug) {
      fetchData();
    }
  }, [slug]);

  let date = new Date(`${game ? game.createdAt : '0000-00-00T00:00:00.000Z'}`);
  let tanggalRilis = `${date.getFullYear()} - ${date.getMonth()} - ${date.getDate()}`;

  return (
    <div className="bg-black">
    <Header />
      <Container>
        <Row>
          <div className="col-lg-12 mb-3">
            <h3 className="fw-bold text-orange">{ game ? game.name : null } </h3>
          </div>
        </Row>

        <Row className="pb-3">
          <div className="col-lg-4">
            <div className="card border-0 bg-black" style={{ width: '20rem' }}>
              <Image
                src={`${game ? (game.thumbnail_url != null ? game.thumbnail_url : 'https://via.placeholder.com/1200') : 'https://via.placeholder.com/1200'}`}
                className="card-img-top"
                style={{ borderRadius: '15px' }}
                alt="poster"
              />
              <div className="card-body">
                <div className="d-grid gap-2">
                    <Link href={`/games/${game ? game.slug : null }/play`}>
                      <a  className="btn btn-primary">
                      Play Now
                      </a>
                    </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-8">
            <Table className="table table-dark table-striped">
              <tbody>
                <tr>
                  <th>Kategori</th>
                  <td className='text-capitalize'>{game ? game.category : '-' }</td>
                </tr>
                <tr>
                  <th>Date Rilis</th>
                  <td>{ tanggalRilis }</td>
                </tr>
                <tr>
                  <th>Total Play</th>
                  <td>{game ? game.play_count : 0 } play</td>
                </tr>
                <tr>
                  <th>Descriptions</th>
                  <td>
                    {
                      game ? game.description : '-'
                    }
                  </td>
                </tr>
                <tr>
                  <th>Production By</th>
                  <td>Universal Studio</td>
                </tr>
              </tbody>
            </Table>
          </div>
        </Row>

        <Row>
        <div className="col-lg-12 pt-3 text-center">
          <h3 className="fw-bold text-orange">Leaderboard</h3>
        </div>
      </Row>

      <Row>
        <Table className="table table-success table-striped">
          <thead>
            <tr>
              <th scope="col" style={{ width: '5%' }}>Rangking</th>
              <th scope="col" style={{ width: '50%' }}>Name</th>
              <th scope="col" style={{ width: '10%' }}>Total Score</th>
              <th scope="col" style={{ width: '30%' }}>Country</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>100</td>
              <td>Thailand</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>80</td>
              <td>Vietnam</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>60</td>
              <td>Indonesia</td>
            </tr>
          </tbody>
        </Table>
      </Row>
      </Container>
    <Footer />
  </div>
  )

}

export default Games
