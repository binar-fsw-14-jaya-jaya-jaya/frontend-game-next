import { Container, Row, Image } from "react-bootstrap";
import Link from "next/link";
import RpsPlay from "../../../components/rockpaperscissors/play";
import withAuth from "../../../checker/withAuth";

const Play = () => (
  <div className="bg-brow">
    <Container>
      <Row>
        <div className="col-lg-12 col-md-12 py-3">
          <Row>
            <div className="col-lg-1 col-md-1 d-flex align-items-center justify-content-center my-2">
              <Link href="/">
                <a title="back">
                  <svg
                    width="36"
                    height="38"
                    viewBox="0 0 36 38"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M35.0039 37.8633L0.09375 22.4648V17.4375L35.0039 0V8.4375L10.957 19.582L35.0039 29.4609V37.8633Z"
                      fill="#724C21"
                    />
                  </svg>
                </a>
              </Link>
            </div>
            <div className="col-lg-1 col-md-1 my-2 d-flex align-items-center justify-content-center">
              <Image src="/assets/images/play_game/logo-1.png" alt="logo-1"/>
            </div>
            <div className="col-lg-10 col-md-10 my-2 d-flex align-items-center title-game ">
              <span className="play-game-title">ROCK PAPER SCISSORS</span>
            </div>
          </Row>
        </div>
      </Row>

      <RpsPlay />
    </Container>
  </div>
);

export default withAuth(Play);
