import React, {useState} from "react";
import { Container, Row, Image } from "react-bootstrap";
import Header from "../../../../components/header";
import Footer from "../../../../components/footer";
import withAuth from "../../../../checker/withAuth";
import { useRouter } from 'next/router';
import { getToken } from "../../../../utils/cookie";
// import React from 'react';
import axios from 'axios';
import { useFormik } from "formik";
import Input from "../../../../formik/input";
import Button from "../../../../formik/button";
import * as Yup from "yup";

const loginSchema = Yup.object().shape({
  firstname: Yup.string().required("First name tidak boleh kosong"),
  lastname: Yup.string().required("Last name tidak boleh kosong"),
  email: Yup.string()
    .email("Format email salah")
    .required("Email tidak boleh kosong"),
  username: Yup.string().required("Username tidak boleh kosong"),
  // password: Yup.string().required("Password tidak boleh kosong").min(6),
  bio: Yup.string().required("Bio tidak boleh kosong"),
  location: Yup.string().required("Negara tidak boleh kosong"),
});


const EditProfil = () => {
  // const router = useRouter()
  // const { id } = router.query;
  // console.log(id)
  const [loading, setLoading] = useState(false);

  const token  = getToken();
  const [profile, setProfile] = useState(null);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [avatar, setAvatar] = useState('');
  const [location, setLocation] = useState('');
  const [bio, setBio] = useState('');
  const [selectedFile, setSelectedFile] = useState('');
  const [previewFile, setPreviewFile] = useState('');

  const onFileChange = event => {
    const file = event.target.files[0];
    if(file){
      setSelectedFile(file);
      setPreviewFile(URL.createObjectURL(file));
    }
  }

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API}/api/myprofile`,{
        headers: {
          'Authorization' : token
        }
      })

      setProfile(response.data.profile);
      setFirstName(response.data.profile.first_name);
      setLastName(response.data.profile.last_name);
      setUserName(response.data.profile.username);
      setEmail(response.data.profile.email);
      setAvatar(response.data.profile.avatar_url);
      setLocation(response.data.profile.location);
      setBio(response.data.profile.bio);
    };

    if (token) {
      fetchData();
    }
  }, [token]);

  const onSubmit = async (values) => {
    setLoading(true);

    const formData = new FormData();
    formData.append("first_name", values.firstname);
    formData.append("last_name",values.lastname);
    formData.append("email",values.email);
    formData.append("username",values.username);
    formData.append("password",values.password);
    formData.append("location",values.location);
    formData.append("bio",values.bio);
    formData.append("avatar", selectedFile);

    let config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    };

    axios.put(`${process.env.NEXT_PUBLIC_API}/api/myprofile/update`, formData, config).then((res) => {
      window.location.href = "/admin/dashboard";
    })
    .catch((error) => {
      console.log(error);
    });
  };


  const formik = useFormik({
    initialValues: {
      firstname: 'harus',
      lastname: "",
      email: "",
      username: "",
      password: "",
      avatar: "",
      bio: "",
      location: "",
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      onSubmit(values);
    },
  });

  formik.initialValues.firstname = firstName ? firstName : '';
  formik.initialValues.lastname = lastName ? lastName : '';
  formik.initialValues.username = userName ? userName: '';
  formik.initialValues.email = email ? email : '';
  formik.initialValues.location = location ? location : '';
  formik.initialValues.bio = bio ? bio : '';

  return (
    <div className="bg-black">
      <Header />
      <Container>
        <Row>
          <div
            className="col-lg-8 offset-lg-2 py-5"
            style={{ paddingTop: "8rem !important" }}
          >
            <div
              className="card"
              style={{
                background: "rgba(112, 108, 104, 0.5)",
                borderRadius: "15px",
              }}
            >
              <div className="card-title text-center py-3">
                <h1
                  className="fw-bold text-uppercase text-white"
                  style={{ letterSpacing: "3px" }}
                >
                  USER PROFIL
                </h1>
              </div>
              <div className="card-body">
                <form onSubmit={formik.handleSubmit}>
                  <Input
                    id="firstname-input"
                    name="firstname"
                    type="text"
                    placeholder="Masukkan first name"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.firstname !== undefined &&
                      formik.touched.firstname
                    }
                    value={formik.values.firstname}
                    label="Your first name"
                    errorMsg={formik.errors.firstname}
                  />
                  <Input
                    id="lastname-input"
                    name="lastname"
                    type="text"
                    placeholder="Masukkan last name"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.lastname !== undefined &&
                      formik.touched.lastname
                    }
                    value={formik.values.lastname}
                    label="Your last name"
                    errorMsg={formik.errors.lastname}
                  />
                  <Input
                    id="username-input"
                    name="username"
                    type="username"
                    placeholder="Masukkan username"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.username !== undefined &&
                      formik.touched.username
                    }
                    value={formik.values.username}
                    label="Your username"
                    errorMsg={formik.errors.username}
                  />
                  <Input
                    id="email-input"
                    name="email"
                    type="email"
                    placeholder="Masukkan email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.email !== undefined && formik.touched.email
                    }
                    value={formik.values.email}
                    label="Your email"
                    errorMsg={formik.errors.email}
                  />
                  <Input
                    id="password-input"
                    name="password"
                    type="password"
                    placeholder="Masukkan password"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.password !== undefined &&
                      formik.touched.password
                    }
                    label="Your password"
                    description="Kosongkan, password jika tidak ingin dirubah"
                    errorMsg={formik.errors.password}
                  />
                    <span className="text-muted">AVATAR</span>
                    <div className="form-floating mb-3">
                    <div className="avatar-top-score d-inline-flex">
                      <span className="avatar-bg" />
                      <Image
                        className="avatar-bg avatar"
                        src={`${previewFile ? previewFile : (avatar ? avatar : 'https://via.placeholder.com/50x50') }`}
                        alt="avatar"
                      />
                    </div>
                    <input
                        type="file"
                        className="form-control"
                        onChange={onFileChange}
                    />
                    <span className="text-muted text-sm-start">Kosongkan, jika avatar tidak ingin dirubah</span>
                    </div>

                  <span className="text-muted">BIODATA</span>
                  <Input
                    id="location-input"
                    name="location"
                    type="text"
                    placeholder="Masukkan password"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.location !== undefined &&
                      formik.touched.location
                    }
                    value={formik.values.location}
                    label="Your country"
                    errorMsg={formik.errors.location}
                  />
                  <Input
                    id="bio-input"
                    name="bio"
                    type="text"
                    placeholder="Masukkan bio datamu"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.bio !== undefined &&
                      formik.touched.bio
                    }
                    value={formik.values.bio}
                    label="Your bio"
                    errorMsg={formik.errors.bio}
                  />
                  <div className="text-center">
                    <Button
                      id="register-submit"
                      type="submit"
                      loading={loading}
                    >
                      SUBMIT
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </Container>
      <Footer />
    </div>
  );
}

export default withAuth(EditProfil);
