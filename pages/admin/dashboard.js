// import React, { Component } from "react";
import Link from "next/link";
import Header from "../../components/header";
import Footer from "../../components/footer";
import { Container, Image, Row } from "react-bootstrap";
import withAuth from "../../checker/withAuth";
import { getToken } from "../../utils/cookie";
import React from 'react';
import axios from 'axios';

const AdminDashboard = () => {
  const token  = getToken();
  const [profile, setProfile] = React.useState(null);

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API}/api/myprofile`,{
        headers: {
          'Authorization' : token
        }
      })

      setProfile(response.data.profile);
    };

    if (token) {
      fetchData();
    }
  }, [token]);

    return (
      <div className="bg-black">
        <Header />
        <Container>
          <Row>
            <div
              className="col-lg-6 py-5"
              style={{ paddingTop: "8rem !important" }}
            >
              <div
                className="card p-4"
                style={{
                  background: "rgba(112, 108, 104, 0.5)",
                  borderRadius: "15px",
                }}
              >
                <div className="row">
                  <div className="col-lg-3 p-2">
                    <div className="avatar-top-score d-inline-flex">
                      <span className="avatar-bg" />
                      <Image
                        className="avatar-bg avatar"
                        src={`${profile ? (profile.avatar_url != null ? profile.avatar_url : 'https://via.placeholder.com/50x50') : 'https://via.placeholder.com/50x50'}`}
                        alt="photo-profile"
                      />
                    </div>
                  </div>

                  <div className="col-lg-9">
                    <div className="d-flex justify-content-between">
                      <div>
                        <span className="fw-bold text-orange">{profile ? `${profile.first_name} ${profile.last_name}` : '-'}</span>
                        <br />
                        <span className="fw-light text-muted">
                          18 year, {profile ? profile.location : 'Indonesia'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <Row>
                  <div className="col-lg-12 p-2">
                    <p className="fw-light text-white">
                      {
                        profile ? profile.bio : '-'
                      }
                    </p>
                    <div className="text-center">
                      <div className="d-grid gap-2">
                        <Link
                          href={`/admin/profil/${profile ? profile.id : 0 }/edit`}
                        >
                          <a className="btn btn-outline-primary btn-sm">
                            Edit Profil
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </Row>
              </div>
            </div>
            <div
              className="col-lg-3 py-5"
              style={{ paddingTop: "8rem !important" }}
            >
              <div
                className="card p-4"
                style={{
                  background: "rgba(112, 108, 104, 0.5)",
                  borderRadius: "15px",
                }}
              >
                <div className="card-title">
                  <h5 className="fw-bold text-orange">Total Wins</h5>
                </div>
                <div className="card-body text-center">
                  <h1
                    className="fw-bold text-white"
                    style={{ fontSize: "70px" }}
                  >
                    50
                  </h1>
                </div>
              </div>
            </div>
            <div
              className="col-lg-3 py-5"
              style={{ paddingTop: "8rem !important" }}
            >
              <div
                className="card p-4"
                style={{
                  background: "rgba(112, 108, 104, 0.5)",
                  borderRadius: "15px",
                }}
              >
                <div className="card-title">
                  <h5 className="fw-bold text-orange">Total Lose</h5>
                </div>
                <div className="card-body text-center">
                  <h1
                    className="fw-bold text-white"
                    style={{ fontSize: "70px" }}
                  >
                    50
                  </h1>
                </div>
              </div>
            </div>
          </Row>

          <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange">Popular Games</h3>
            </div>
          </Row>

          <Row className="row">
            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center py-2">
              <div
                className="card border-0 bg-black"
                style={{ width: "18rem" }}
              >
                <Image
                  src="https://via.placeholder.com/150"
                  className="card-img-top"
                  style={{ borderRadius: "15px" }}
                  alt="poster"
                />
                <div className="card-body">
                  <h5 className="card-title text-white">Card title</h5>
                </div>
              </div>
            </div>
          </Row>
        </Container>
        <Footer />
      </div>
    );
}

export default withAuth(AdminDashboard);
