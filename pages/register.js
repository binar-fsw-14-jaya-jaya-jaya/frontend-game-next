import React, { useState } from "react";
import { Container, Row, Image } from "react-bootstrap";
import { setToken } from "../utils/cookie";
import { useFormik } from "formik";
import Input from "../formik/input";
import Button from "../formik/button";
import * as Yup from "yup";
import axios from "axios";
import Link from "next/link";

const loginSchema = Yup.object().shape({
  firstname: Yup.string().required("First name tidak boleh kosong"),
  lastname: Yup.string().required("Last name tidak boleh kosong"),
  email: Yup.string()
    .email("Format email salah")
    .required("Email tidak boleh kosong"),
  username: Yup.string().required("Username tidak boleh kosong"),
  password: Yup.string().required("Password tidak boleh kosong").min(6),
});

function Register() {
  const [loading, setLoading] = useState(false);

  const onSubmit = async (values) => {
    setLoading(true);

    let formData = {
      first_name: values.firstname,
      last_name: values.lastname,
      email: values.email,
      username: values.username,
      password: values.password,
    };

    axios
      .post(`${process.env.NEXT_PUBLIC_API}/api/register`, formData)
      .then((res) => {
        setToken(res.data.user.accessToken);
        window.location.href = "/";
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const formik = useFormik({
    initialValues: {
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: (values) => {
      onSubmit(values);
    },
  });
  // render() {
  return (
    <div className="bg-black">
      <Container>
        <Row>
          <div className="col-lg-4 offset-lg-4 py-5">
            <div
              className="card"
              style={{
                background: "rgba(112, 108, 104, 0.5)",
                borderRadius: "15px",
              }}
            >
              <div className="card-title text-center py-3">
                <div className="text-center">
                  <Link href={'/'} passHref>
                    <a>
                      <Image
                        src="/assets/images/logo.svg"
                        className="w-25 h-25 m-3"
                        alt="logo"
                      />
                    </a>
                  </Link>
                </div>
                <h1
                  className="fw-bold text-uppercase text-white"
                  style={{ letterSpacing: "3px" }}
                >
                  Register
                </h1>
              </div>
              <div className="card-body">
                <form onSubmit={formik.handleSubmit}>
                  <Input
                    id="firstname-input"
                    name="firstname"
                    type="text"
                    placeholder="Masukkan username"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.firstname !== undefined &&
                      formik.touched.firstname
                    }
                    label="Your first name"
                    errorMsg={formik.errors.firstname}
                  />
                  <Input
                    id="lastname-input"
                    name="lastname"
                    type="text"
                    placeholder="Masukkan username"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.lastname !== undefined &&
                      formik.touched.lastname
                    }
                    label="Your last name"
                    errorMsg={formik.errors.lastname}
                  />
                  <Input
                    id="email-input"
                    name="email"
                    type="email"
                    placeholder="Masukkan email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.email !== undefined && formik.touched.email
                    }
                    label="Your email"
                    errorMsg={formik.errors.email}
                  />
                  <Input
                    id="username-input"
                    name="username"
                    type="username"
                    placeholder="Masukkan username"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.username !== undefined &&
                      formik.touched.username
                    }
                    label="Your username"
                    errorMsg={formik.errors.username}
                  />
                  <Input
                    id="password-input"
                    name="password"
                    type="password"
                    placeholder="Masukkan password"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.errors.password !== undefined &&
                      formik.touched.password
                    }
                    label="Your password"
                    errorMsg={formik.errors.password}
                  />
                  <p className="my-2 text-white">
                    Already have an account?
                    <Link href="/login">
                      <a className="text-decoration-none mx-2">Let's login</a>
                    </Link>
                  </p>
                  <div className="text-center">
                    <Button
                      id="register-submit"
                      type="submit"
                      loading={loading}
                    >
                      Register
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
}
// }

export default Register;
