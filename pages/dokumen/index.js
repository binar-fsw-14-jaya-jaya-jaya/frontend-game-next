import React, { useEffect, useRef } from "react";
import { Container, Row  } from "react-bootstrap";
import Footer from "../../components/footer";
import Header from "../../components/header";

export default function App() {
  const containerRef = useRef(null);

  useEffect(() => {
    const container = containerRef.current;
    let PSPDFKit;

    (async function () {
      PSPDFKit = await import("pspdfkit");
      await PSPDFKit.load({
        container,
        document: "/Document.pdf",
        baseUrl: `${window.location.protocol}//${window.location.host}/`,
      });
    })();

    return () => PSPDFKit && PSPDFKit.unload(container);
  }, []);

  return (
    <div className="bg-black">
        <Header />
        <Container>
          <Row>
            <div className="col-lg-12 px-5">
              <div ref={containerRef} style={{ height: "100vh" }} />
              <style global jsx>
                {`
                  * {
                    margin: 0;
                    padding: 0;
                  }
                `}
              </style>
            </div>
          </Row>
        </Container>
        <Footer />
    </div>
  );
}
