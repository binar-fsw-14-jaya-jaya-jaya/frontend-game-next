import React, { Component } from 'react';
import { Container, Row, Image } from "react-bootstrap";
import Footer from "../../components/footer";
import Header from "../../components/header";
import { Player, ControlBar,  BigPlayButton, ForwardControl } from 'video-react';
import axios from "axios";

class Index extends Component {

  constructor(props) {
    super(props);

    let videos  =  props.data.videos;

    this.state = {
      source: videos[0].url,
      titleVideo: videos[0].title,
      posterVideo: videos[0].poster,
      videoss: videos
    };
  }

  changeSource(url, title, poster) {
    return () => {
      this.setState({
        source: url,
        titleVideo: title,
        posterVideo: poster
      });
      this.player.load();
    }
  }

  render(){
    return (
      <div className="bg-black">
        <Header />
          <Container>
           <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange my-3 text-center">{this.state.titleVideo}</h3>
              <div className='d-flex justify-content-center'>
              <Player
                ref={player => {
                  this.player = player
                }}
                poster={this.state.posterVideo}
                width={640}
                height={360}
                fluid={false}
                volume={0.30}
              >
                <source src={this.state.source} />
                <ControlBar autoHide={true}>
                  <ForwardControl seconds={5} order={3.1} />
                  <ForwardControl seconds={10} order={3.2} />
                  <ForwardControl seconds={30} order={3.3} />
                </ControlBar>
                <BigPlayButton position="center" />
              </Player>
              </div>

            </div>
          </Row>

        <Row>
            <div className="col-lg-12">
              <h3 className="fw-bold text-orange my-3">See Other Videos</h3>
            </div>
          </Row>

          <Row>
            {
              this.state.videoss.map(video => (
                <div
                  key={`video-${video.id}`}
                  className="col-lg-2 col-md-4 col-sm-6 d-flex justify-content-center py-2"
                  onClick={this.changeSource(`${video.url}`, `${video.title}`, `${video.poster}`)}
                  style={{ cursor: 'pointer' }}>
                  <div className="card border-0 bg-black" style={{ width: '18rem' }} >
                    <Image src={video.poster} className="card-img-top" style={{ borderRadius: '15px' }} alt="poster"/>
                    <div className="card-body">
                      <a className="text-decoration-none">
                        <h5 className="card-title text-white">{video.title}</h5>
                      </a>
                    </div>
                  </div>
                </div>
              ))
            }

          </Row>

          </Container>
        <Footer />
      </div>
    )
  }
}

export default Index;

export const getStaticProps = async () => {
  const res = await axios.get(`${process.env.NEXT_PUBLIC_API}/api/videos`);

  return {
    props: { data: res.data }
  }
}