import * as React from 'react';

import "bootstrap/dist/css/bootstrap.css";
import "../styles/additional.css";
import "../styles/additional_2.css";
import { Provider } from "react-redux";
import store from "../redux/store";
import "video-react/dist/video-react.css";

export default function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}
