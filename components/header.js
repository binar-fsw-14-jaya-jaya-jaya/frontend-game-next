import React from "react";
import { Nav, Navbar, Container, Image, Dropdown } from 'react-bootstrap';
import Link from 'next/link';
import { getToken, removeToken } from "../utils/cookie";

export default function Header() {
  const token = getToken();

  const logout = () => {
    removeToken();
    window.location.href ="/";
  }

  return (
    <Navbar expand="lg" variant="dark" className="navbar-dark-transparent">
      <Container>
        <Navbar.Brand href="#" className="navbar-brand mx-5">
          <Image src="/assets/images/logo.svg" alt="logo"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto mb-2 mb-lg-0">
            <Link href="/">
              <a className="nav-link text-white mx-3">HOME</a>
            </Link>
            <Link href="/games">
              <a className="nav-link text-white mx-3" >GAMES</a>
            </Link>
            <Link href="/videos">
              <a className="nav-link text-white mx-3" >VIDEO</a>
            </Link>
            <Link href="/dokumen">
              <a className="nav-link text-white mx-3">DOKUMEN</a>
            </Link>
            <Link href="#">
              <a className="nav-link text-white mx-3">ABOUT ME</a>
            </Link>
          </Nav>
          <div className="d-flex">
            <Nav className="me-auto mb-2 mb-lg-0">
              <Dropdown className="mx-3 border-right">
                <Dropdown.Toggle
                  className="bg-transparent border-0 ps-0"
                  id="dropdown-basic"
                  style={{outline: 'none', boxShadow: 'none' }}>
                  AKUN
                </Dropdown.Toggle>
                <Dropdown.Menu className="dropdown-menu-dark">
                  {
                    !token
                      ? (
                        <>
                          <Link href="/login">
                            <a className="dropdown-item">LOGIN</a>
                          </Link>
                          <Link href="/register">
                            <a className="dropdown-item">REGISTER</a>
                          </Link>
                        </>
                      )
                      : (
                        <>
                          <Link href="/admin/dashboard">
                            <a className="dropdown-item">DASHBOARD</a>
                          </Link>
                          <Dropdown.Item href="#" onClick={logout}>LOGOUT</Dropdown.Item>
                        </>
                      )
                  }
                </Dropdown.Menu>
              </Dropdown>
            </Nav>

            <a className="mx-3" href="#" style={{ textDecoration: 'none' }}>
              <Image src="/assets/images/xbox.svg" className="img-fluid" alt="x-box"/>
            </a>
            <a className="mx-3" href="#" style={{ textDecoration: 'none' }}>
              <Image src="/assets/images/steam.svg" className="img-fluid" alt="steam"/>
            </a>
          </div>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}