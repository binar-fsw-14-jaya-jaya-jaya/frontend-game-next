import React, { useMemo, useState } from "react";
import { Row, Image } from "react-bootstrap";

function RpsPlay () {
  const [selected, setSelected] = useState("");
  const [computedSelected, setComputedSelected] = useState("");
  const [refresh, setRefresh] = useState(true);

  const result = useMemo(() => {
    if (selected) {
      const choices = ["batu", "kertas", "gunting"];
      const computerChoiceIndex = Math.floor(Math.random() * choices.length);
      setComputedSelected(choices[computerChoiceIndex]);

      if (computedSelected === selected) {
        return `draw`;
      } else {
        if (
          (computedSelected === "batu" && selected === "gunting") ||
          (computedSelected === "kertas" && selected === "batu") ||
          (computedSelected === "gunting" && selected === "kertas")
        ) {
          return "com win";
        }
        return "player 1 win";
      }
    }
  }, [computedSelected, selected]);

  const Refresh = () => {
    setRefresh(!refresh);
    setComputedSelected("");
    setSelected("");
  }

  return (
    <>
      <Row className="pt-5">
        <div className="col-lg-4 col-md-12 d-flex justify-content-center">
            <Row className="g-2 player-satu">
              <div className="col-12">
                <div className="p-3 text-center play-game-subtitle">PLAYER 1</div>
              </div>
              <div className="col-12 text-center">
                <div className="py-4 d-flex justify-content-center">
                  <button
                    className={`
                      btn btn-play-game
                      d-flex
                      justify-content-center
                      align-items-center
                      ${selected === "batu" ? `active` : ""}
                      ${result ? "disabled" : ""}
                    `}
                    style={{
                      backgroundImage: 'url(/assets/images/play_game/batu.png)',
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'center',
                      backgroundSize: '115px 150px',
                    }}
                    title="batu"
                    onClick={() => setSelected("batu")}
                  >
                  </button>
              </div>
            </div>
            <div className="col-12 text-center">
              <div className="py-4 d-flex justify-content-center">
                <button
                    className={`
                      btn btn-play-game
                      d-flex
                      justify-content-center
                      align-items-center
                      ${selected === "kertas" ? `active` : ""}
                      ${result ? "disabled" : ""}
                    `}
                    style={{
                      backgroundImage: 'url(/assets/images/play_game/kertas.png)',
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'center',
                      backgroundSize: '115px 150px',
                    }}
                    title="kertas"
                    onClick={() => setSelected("kertas")}
                  >
                  </button>

              </div>
            </div>
            <div className="col-12 text-center">
              <div className="py-4 d-flex justify-content-center">
                  <button
                    className={`
                      btn btn-play-game
                      d-flex
                      justify-content-center
                      align-items-center
                      ${selected === "gunting" ? `active` : ""}
                      ${result ? "disabled" : ""}
                    `}
                    style={{
                      backgroundImage: 'url(/assets/images/play_game/gunting.png)',
                      backgroundRepeat: 'no-repeat',
                      backgroundPosition: 'center',
                      backgroundSize: '115px 150px',
                    }}
                    title="gunting"
                    onClick={() => setSelected("gunting")}
                  >
                  </button>
              </div>
            </div>
          </Row>
        </div>
        <div className="col-lg-4 col-md-12 d-flex justify-content-center align-items-center">
          <Row>
            <div className="col-lg-12">
              <div
                className={
                  `p-3
                  d-flex
                  justify-content-center
                  align-items-center
                  ${
                    (result === "com win" || result === 'player 1 win') ? `play-game-result`
                      : result === "draw" ? `draw play-game-result`
                      : " play-game-vs"
                  }`
                }
              >
                <span className={
                  `${
                    (result === "com win" || result === 'player 1 win' || result === 'draw')
                      ? `result-text`
                      : ""
                  }`
                }>
                  {result ? result : 'VS' }
                </span>
              </div>
            </div>
          </Row>
        </div>

        <div className="col-lg-4 col-md-12 d-flex justify-content-center">
          <Row className="g-2" id="computerId">
            <div className="col-lg-12">
              <div className="p-3 text-center play-game-subtitle">COM</div>
            </div>
            <div className="col-12 text-center">
              <div className="py-4 d-flex justify-content-center">
                <div
                  className={
                    `btn btn-play-game
                    d-flex
                    justify-content-center
                    align-items-center
                    ${computedSelected === "batu" ? `active` : ""}
                    ${result ? "disabled" : ""}
                    `
                  }
                >
                  <Image
                    className="play-game-icon-size"
                    src="/assets/images/play_game/batu.png"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="col-12 text-center">
              <div className="py-4 d-flex justify-content-center">
                <div
                  className={
                    `btn btn-play-game
                    d-flex
                    justify-content-center
                    align-items-center
                    ${computedSelected === "kertas" ? `active` : ""}
                    ${result ? "disabled" : ""}
                    `
                  }
                >
                  <Image
                    className="play-game-icon-size"
                    src="/assets/images/play_game/kertas.png"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="col-12 text-center">
              <div className="py-4 d-flex justify-content-center">
                <div
                  className={
                    `btn btn-play-game
                    d-flex
                    justify-content-center
                    align-items-center
                    ${computedSelected === "gunting" ? `active` : ""}
                    ${result ? "disabled" : ""}
                    `
                  }
                >
                  <Image
                    className="play-game-icon-size"
                    src="/assets/images/play_game/gunting.png"
                    alt=""
                  />
                </div>
              </div>
            </div>
          </Row>
        </div>
      </Row>

      <Row className="pb-5">
        <div className="col-lg-12 col-md-12 d-flex align-items-center justify-content-center">

            <div className={`pe-auto refresh ${!refresh ? `refresh-rotate` : ""}`}>
              <Image
                src="/assets/images/play_game/refresh.png"
                alt="Refresh"
                title="refresh"
                onClick={Refresh}
              />
            </div>
        </div>
      </Row>
    </>
  )
}

export default RpsPlay;