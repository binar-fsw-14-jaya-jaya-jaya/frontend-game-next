import React from "react";
import { Container, Image, Row } from "react-bootstrap";
// import { Link } from "react-router-dom";

const LpSubscribe = () => {
  return (
    <>
      <section className="subscribe">
        <Container className="px-4 px-lg-5">
          <Row>
            <div className="col-lg-5 col-md-5 col-sm-12">
              <div className="d-flex justify-content-end">
                <Image
                  src="../../assets/images/subscribe-image.png"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
            <div className="col-lg-7 col-md-7 col-sm-12 mt-5">
              <div className="text-white" style={{ padding: '1rem 1.25rem' }}>
                <h3 className="subscribe-subtitle">Want to stay in <br /> touch?</h3>
                <h1 className="subscribe-title">
                  newsletter SUBSCRIBE
                </h1>
                <p className="fw-light">
                  In order to start receiving our news, all you have to do is
                  enter your email address. Everything else will be taken care of
                  by us. We will send you emails containing information about
                  game. We don’t spam.
                </p>
              </div>
              <div className="row" style={{ padding: '1rem 1.25rem' }}>
                <div className="col-lg-6 col-md-6">
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="name"
                      type="text"
                      placeholder="username@email.com"
                      data-sb-validations="required"
                      autoComplete="off"
                    />
                    <label>Your email addres</label>
                    <div
                      className="invalid-feedback"
                      data-sb-feedback="name:required"
                    >
                      A name is required.
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6">
                  <div className="d-grid">
                    <div className="subscribe-button">
                      <button
                      className="btn btn-xl"
                      style={{ height: 'calc(3.5rem + 2px)', lineHeight: '1.25' }}
                      type="submit"
                    >
                      Subscribe now
                    </button>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </Row>
          <Row>
            <div className="col-lg-10 offset-lg-2 col-md-12 mt-4">
              <Row>
                <div className="col-lg-9 col-md-12 my-3 subscribe-footer">
                  <div className="row text-center">
                    <div className="col-lg-1 my-3">
                      <a href="#home" className="text-white text-decoration-none">
                        Main
                      </a>
                    </div>
                    <div className="col-lg-2 my-3">
                      About
                    </div>
                    <div className="col-lg-3 my-3">
                      <a href="#gameChoice" className="text-white text-decoration-none">
                        Game Features
                      </a>
                    </div>
                    <div className="col-lg-4 my-3">
                      <a href="#requirements" className="text-white text-decoration-none">
                        System Requirements
                      </a>
                    </div>
                    <div className="col-lg-2 my-3">
                      <a href="#topScore" className="text-white text-decoration-none">
                        Quotes
                      </a>

                    </div>
                  </div>

                </div>

                <div className="col-lg-3 col-md-12 my-3">
                  <div className="row text-center">
                    <div className="col-3 my-3">
                      <Image src="/assets/images/facebook.svg"alt="" />
                    </div>
                    <div className="col-3 my-3">
                      <Image src="/assets/images/twitter-2.svg"alt="" />
                    </div>
                    <div className="col-3 my-3">
                      <Image src="/assets/images/youtube.svg"alt="" />
                    </div>
                    <div className="col-3 my-3">
                      <Image src="/assets/images/twitch.svg"alt="" />
                    </div>
                  </div>
                </div>
              </Row>
            </div>
          </Row>
        </Container>
      </section>
    </>
  )
}

export default LpSubscribe