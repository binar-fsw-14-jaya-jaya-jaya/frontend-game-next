import React from "react";
import { Container, Image } from "react-bootstrap";
// import { Link } from "next/link";

const LpTopScore = () => {
  return (
    <>
      <section className="py-base top-score" id="topScore">
        <Container className=" px-4 px-lg-5">
          <div className="row">
            <div className="col-lg-5">
              <div
                className="
                  row
                  gx-lg-5
                  h-100
                  align-items-center
                  justify-content-center
                "
              >
                <div className="col-lg-12 col-sm-12 top-50 start-50">
                  <h1 className="top-score-title">Top Scores</h1>
                  <h3 className="top-score-subtitle">
                    This top score from various games provided on this platform
                  </h3>
                  <div className="top-score-button">
                    <a className="btn btn-xl my-3" href="#">See More</a>
                  </div>
                </div>

              </div>
            </div>
            <div className="col-lg-7">
              <div className="col-lg-8 offset-md-4">
                <div className="card p-4 bg-dark">
                  <div className="row">
                    <div className="col-lg-3 p-2">
                      <div className="avatar-top-score d-inline-flex">
                        <span className="avatar-bg" />
                        <Image className="avatar-bg avatar" src="../../assets/images/avatar/avatar-1.jpg" alt="avatar-1"/>
                      </div>
                    </div>
                    <div className="col-lg-9">
                      <div className="d-flex justify-content-between">
                        <div>
                          <span className="fw-bold text-orange">Evan Lahti</span
                          ><br />
                          <span className="fw-light text-muted">PC Gamer</span>
                        </div>
                        <div>
                          <Image
                            src="../../assets/images/twitter.svg"
                            className="img-fluid"
                            alt="twitter"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 p-2">
                      <p className="fw-light text-white">
                        "One of my highlights of the year."
                      </p>
                      <span className="fw-light text-muted">Oktober 10, 2020</span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-8 offset-md-2 mt-4">
                <div className="card p-4 bg-dark">
                  <div className="row">
                    <div className="col-lg-3 p-2">
                      <div className="avatar-top-score d-inline-flex">
                        <span className="avatar-bg" />
                        <Image className="avatar-bg avatar" src="../../assets/images/avatar/avatar-2.jpg" alt="avatar-2"/>
                      </div>
                    </div>
                    <div className="col-lg-9">
                      <div className="d-flex justify-content-between">
                        <div>
                          <span className="fw-bold text-orange">Jadda Griffin</span
                          ><br />
                          <span className="fw-light text-muted">Nedreactor</span>
                        </div>
                        <div>
                          <Image
                            src="../../assets/images/twitter.svg"
                            className="img-fluid"
                            alt="twitter"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 p-2">
                      <p className="fw-light text-white">
                        "One of my highlights of the year."
                      </p>
                      <span className="fw-light text-muted">Oktober 10, 2020</span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-8 offset-md-4 mt-4">
                <div className="card p-4 bg-dark">
                  <div className="row">
                    <div className="col-lg-3 p-2">
                      <div className="avatar-top-score d-inline-flex">
                        <span className="avatar-bg" />
                        <Image className="avatar-bg avatar" src="../../assets/images/avatar/avatar-3.jpg" alt="avatar-3"/>
                      </div>
                    </div>
                    <div className="col-lg-9">
                      <div className="d-flex justify-content-between">
                        <div>
                          <span className="fw-bold text-orange">Aaron Williams</span
                          ><br />
                          <span className="fw-light text-muted">Uproxx</span>
                        </div>
                        <div>
                          <Image
                            src="../../assets/images/twitter.svg"
                            className="img-fluid"
                            alt="twitter"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 p-2">
                      <p className="fw-light text-white">
                        "One of my highlights of the year."
                      </p>
                      <span className="fw-light text-muted">Oktober 10, 2020</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </section>
    </>
  )
}

export default LpTopScore;