import { Container, Row } from "react-bootstrap";

export default function Footer() {
  return (
    <footer className="bg-footer py-3">
      <Container className="px-4 px-lg-5">
        <Row className="bg-footer-card">
          <div className="col-lg-4 py-2 ps-0">
            <div className="copyright">Copyright &copy; 2022 - Challenge Chapter 9</div>
          </div>
          <div className="col-lg-3 d-none d-lg-block py-2"></div>
          <div className="col-lg-5 py-2 pe-0">
            <div className="disclaimer">
              <span>Privacy Policy |</span>
              <span>Terms Of Services |</span>
              <span>Code of Conduct</span>
            </div>
          </div>
        </Row>
      </Container>
    </footer>
  )
}
